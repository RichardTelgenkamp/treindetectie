import math

class Value:

    def __init__(self, time, x, y, z):
        self.time = time
        self.x = x
        self.y = y
        self.z = z
        self.vector = math.sqrt(math.pow(x,2) + math.pow(y,2) + math.pow(z,2))
