# from machine import SD
# import sqnsupgrade
#
#
# sd = SD()
# os.mount(sd, '/sd')
# sqnsupgrade.run('/sd/NB1-37781.dup','/sd/updater.elf')

import os
import machine
import gc
from machine import SD
from machine import RTC
from machine import WDT

import time
import pycom
import ubinascii
import machine
from network import WLAN

from network import LTE
from network import Bluetooth
from train_detection import TrainDetection
from api import Api
from settings import Settings

# Colors
off = 0x000000
red = 0xff0000
green = 0x00ff00
blue = 0x0000ff
yellow = 0xffff00

print('Started')
wdt = WDT(timeout=30000)
sd = SD()
try:
    os.mount(sd, '/sd')
except:
    print('Mount failed')

device_id = ubinascii.hexlify(machine.unique_id()).decode("utf-8") .upper()
rtc = RTC()
rtc.init((2018,9,28,8,0,0,0,0))
wlan = WLAN()
lte=LTE()
wlan.deinit()
#wlan = WLAN(mode=WLAN.STA)
trainDetection = TrainDetection(device_id, rtc)
status_dict = dict()
status_dict['device_id'] = device_id

def update_status():
    dt = rtc.now()
    status_dict['time'] = '{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}.{:06d}Z'.format(dt[0],dt[1],dt[2],dt[3],dt[4],dt[5],dt[6])
    status_dict['uptime'] = time.ticks_ms()
    status_dict['buffer_count'] = len(trainDetection.upload.measurements)

def setup_lte_network():

    print('Setup modem lte...')
    #kpn setup
    lte.send_at_cmd('AT+CFUN=0') # disable modem
    lte.send_at_cmd('AT!="clearscanconfig"') # clear scanned frequencies
    lte.send_at_cmd('AT!="addscanfreq band=20 dl-earfcn=6400"') # set scanned frequency
    lte.send_at_cmd('AT+CFUN=1') # enable modem

    print('Attaching lte...')
    lte.attach()

    while not lte.isattached():
        for x in range(20):
            pycom.rgbled(red)
            time.sleep(0.25)
            pycom.rgbled(off)
            print('Attaching...')
            if lte.isattached():
                break
        break
    if lte.isattached():
        lte.connect()
        while not lte.isconnected():
            pycom.rgbled(yellow)
            time.sleep(0.25)
            pycom.rgbled(off)
            print('Connecting...')

        print('Connected')
    else:
        print('Not attached ')



    # print('Searching network...')
    # nets = wlan.scan()
    # for net in nets:
    #     if net.ssid == Settings.MAIN_WLAN_SSID:
    #         print('Network found')
    #         print('Connecting...')
    #         # print(net.sec)
    #         # print(WLAN.WEP)
    #         # print(WLAN.WPA)
    #         # print(WLAN.WPA2)
    #         wlan.connect(net.ssid, auth=(net.sec, Settings.MAIN_WLAN_PASSWORD), timeout=20000)
    #         for x in range(20):
    #             time.sleep(1)
    #             if wlan.isconnected():
    #                 break
    #         break
    #
    if lte.isconnected():
         print('LTE-M connection succeeded')
         #ip = lte.ifconfig()[0]
         #print('IP Address: {}'.format(ip))
         print('Syncing RTC...')
         rtc.ntp_sync("pool.ntp.org")
         for x in range(10):
             time.sleep(1)
             if rtc.synced():
                 break

         if rtc.synced():
             print('RTC synced')
             #return True
         else:
             print('RTC not synced')
    else:
         print('LTE connection failed RTC not synced with network')
    return True

if setup_lte_network():
    api = Api()
    # downloads = api.get_downloads(device_id)
    # if len(downloads) > 0:
    #     print('Processing downloads...')
    #     for id in downloads:
    #         download = api.get_download(id)
    #         if download != None:
    #             print('Download OK:{}'.format(download['fileName']))
    #             with open(download['fileName'], 'w') as f:
    #                 f.write(download['fileContent'])
    #         else:
    #             print('Download failed')
    #
    #     print('Resetting...')
    #     machine.reset()
    # else:
    
    print('Running train detection...')
    trainDetection.start()

    statusCounter = Settings.MAIN_POST_STATUS_INTERVAL
    while True:
        # print('MEM: {}/{}'.format(gc.mem_alloc(), gc.mem_free()))
        gc.collect()
        if statusCounter == 0:
            statusCounter = Settings.MAIN_POST_STATUS_INTERVAL
            update_status()
            result = api.post_status(status_dict)
            if result != None and result['reboot']:
                machine.reset()
        else:
            statusCounter -= 1

        #if wlan.isconnected():
        #    if trainDetection.runningFlag:
        wdt.feed()
        #        trainDetection.runningFlag = False
        #else:
        #    setup_network()

        time.sleep(1)
        #machine.deepsleep(20000)



# print('Starting...')
# lte = LTE()
# print('Attaching...')
# lte.attach()
# print('Waiting...')
#
# for x in range(30):
#     time.sleep(2)
#
#     if lte.isattached():
#         break
#         print('Attached!')
#     else:
#         print('Not attached {}'.format(x))
#
# lte.deinit()

# while not lte.isattached():
#     time.sleep(0.5)
#     print('Attaching...')
#
# lte.connect(cid=3)
# while not lte.isconnected():
#     time.sleep(0.5)
#     print('Connecting...')
#
# print('Connected')
