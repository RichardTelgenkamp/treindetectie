import utime
import pycom
import _thread
import time
from machine import Pin
from machine import Timer
from sensor import Sensor
from detector import Detector
from value import Value
from upload import Upload
from api import Api
from settings import Settings



class TrainDetection:
    def __init__(self, device_id, rtc):
        pycom.heartbeat(False)
        self.device_id = device_id
        self.rtc = rtc
        self.api = Api()
        self.sensor = Sensor()
        self.upload = Upload(self.api)
        self.detector = Detector(self)
        self.alarm = None
        self.runningFlag = False
        print("Device ID : " + str(device_id))

    def start(self):
        self.upload.start()
        self.alarm = Timer.Alarm(self.do_measurement, ms=Settings.TRAIN_DETECTION_READ_INTERVAL, periodic=True)

    def do_measurement(self, alarm):
        value = self.sensor.get_value()
        accelvalue = self.sensor.get_accelvalue()
        self.detector.check_train(accelvalue)
        self.detector.add_value(value)
        self.runningFlag = True
        
