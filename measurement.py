
import os
class Measurement:

    def __init__(self, device_id, time):
        self.device_id = device_id
        self.values = list()
        self.time = time
        self.file_name = ''

    def add_value(self, val):
        self.values.append(val)

    def write_to_file(self, dir):
        dt = self.time
        self.file_name = '{}/{:04d}{:02d}{:02d}_{:02d}{:02d}{:02d}_{:06d}.txt'.format(dir, dt[0],dt[1],dt[2],dt[3],dt[4],dt[5],dt[6])

        with open(self.file_name, 'w') as f:
            f.write('{\r\n')
            f.write('\t\"time\" : \"{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}.{:06d}Z\",\r\n'.format(dt[0],dt[1],dt[2],dt[3],dt[4],dt[5],dt[6]))
            f.write('\t\"device_id\" : \"{}\",\r\n'.format(self.device_id))
            f.write('\t\"values\" : [\r\n')
            n = len(self.values)
            for i in range(n):
                f.write('\t{\r\n')
                f.write('\t\t\"time\" : {},\r\n'.format(self.values[i].time))
                f.write('\t\t\"x\" : {},\r\n'.format(self.values[i].x))
                f.write('\t\t\"y\" : {},\r\n'.format(self.values[i].y))
                f.write('\t\t\"z\" : {},\r\n'.format(self.values[i].z))
                if i < n - 1:
                    f.write('\t},\r\n')
                else:
                    f.write('\t}]\r\n')

            f.write('}')
        #print("File written")
        #except Exception as e:
        #    print("Write to file error: " + str(e))
        # with open(self.file_name, 'r') as f:
        #     print(f.read())
