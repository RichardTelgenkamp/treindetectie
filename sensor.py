from machine import I2C
import utime
from value import Value
from accvalue import AccValue

class Sensor:

    #magnetic sensor
    MAG_ADDRESS = 0x1E
    MAG_REG_CTRL1 = 0x20
    MAG_REG_CTRL2 = 0x21
    MAG_REG_CTRL3 = 0x22
    MAG_REG_CTRL4 = 0x23
    MAG_REG_X = 0x28
    MAG_REG_Y = 0x2A
    MAG_REG_Z = 0x2C
    #accelleration sensor
    ACC_ADDRESS = 0x6B
    ACC_REG_CTRL_5XL = 0x1F
    ACC_REG_CTRL_6XL = 0x20
    ACC_REG_X = 0x28
    ACC_REG_Y = 0x2A
    ACC_REG_Z = 0x2C

    def __init__(self):
        self.i2c = I2C(0, I2C.MASTER, baudrate=100000)
        try:
            self.i2c.writeto_mem (Sensor.MAG_ADDRESS, Sensor.MAG_REG_CTRL2, 0b00001100) #RESET, RANGE 4 GAUSS
            self.i2c.writeto_mem (Sensor.MAG_ADDRESS, Sensor.MAG_REG_CTRL1, 0b01111110) #HIGH PERFORMANCE X Y, HIGH OUTPUT RATE
            self.i2c.writeto_mem (Sensor.MAG_ADDRESS, Sensor.MAG_REG_CTRL4, 0b00001100) #HIGH PERFORMANCE Z
            self.i2c.writeto_mem (Sensor.MAG_ADDRESS, Sensor.MAG_REG_CTRL3, 0b00000000) #CONTINUOUS MODE

            # Enable the accelerometer continous
            self.i2c.writeto_mem(Sensor.ACC_ADDRESS, Sensor.ACC_REG_CTRL_5XL, 0x38)
            self.i2c.writeto_mem(Sensor.ACC_ADDRESS, Sensor.ACC_REG_CTRL_6XL, 0xC0)
        except Exception as e:
            print("I2cinit error: " + str(e))

    def convert_value(low, high):
        val = (high << 8) + low
        if(val > 0x8000):
            val -= 0x10000
        return val

    def get_value(self):
        bs = self.i2c.readfrom_mem(Sensor.MAG_ADDRESS, Sensor.MAG_REG_X, 6)
        val_x = Sensor.convert_value(bs[0], bs[1])
        val_y = Sensor.convert_value(bs[2], bs[3])
        val_z = Sensor.convert_value(bs[4], bs[5])
        time = utime.ticks_ms()
        return Value(time, val_x, val_y, val_z)

    def get_accelvalue(self):
        bs = self.i2c.readfrom_mem(Sensor.ACC_ADDRESS, Sensor.ACC_REG_X, 6)
        val_acc_x = Sensor.convert_value(bs[0], bs[1])
        val_acc_y = Sensor.convert_value(bs[2], bs[3])
        val_acc_z = Sensor.convert_value(bs[4], bs[5])
        time = utime.ticks_ms()
        return AccValue(time, val_acc_x, val_acc_y, val_acc_z)
