import math

class AccValue:

    def __init__(self, time, x, y, z):
        self.time = time
        self.x = x
        self.y = y
        self.z = z
        self.pitch = self.pitch()
        self.roll = self.roll()
        

    def roll(self):
        rad = math.atan2(-self.x, self.z)
        return (180 / math.pi) * rad

    def pitch(self):
        rad = -math.atan2(self.y, (math.sqrt(self.x*self.x + self.z*self.z)))
        return (180 / math.pi) * rad
