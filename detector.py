from settings import Settings
from measurement import Measurement
from machine import Timer

from statusled import StatusLed
import pycom
import time



class Detector:



    def __init__ (self, train_detection):
        #magnetic sensor
        self.mag_buffer = list()
        self.mag_measurement = None
        self.mag_holding_counter = 0
        self.mag_sumLeft = 0
        self.mag_sumRight = 0
        #accelerometer
        self.acc_buffer = list()
        self.acc_measurement = None
        self.acc_holding_counter = 0
        self.acc_sumLeft = 0
        self.acc_sumRight = 0
        self.train_detected_accel = False

        self.statusled = StatusLed(self)
        self.train_detection = train_detection



    def check_train(self, accvalue):
        #print("accel counter : " + '{}'.format(self.acc_holding_counter))
        #check vibration on x-axis to detect trainDetection
        self.acc_buffer.append(accvalue)
        bufferLength = len(self.acc_buffer)
        if bufferLength < Settings.DETECTOR_BUFFER_SIZE / 2:
            self.acc_sumLeft += accvalue.y
        elif bufferLength <= Settings.DETECTOR_BUFFER_SIZE:
            self.acc_sumRight += accvalue.y
        else:
            #UPDATE BUFFER
            removeLeft = self.acc_buffer[0]
            removeRight = self.acc_buffer[int(Settings.DETECTOR_BUFFER_SIZE / 2)]
            self.acc_buffer.pop(0)

            #UPDATE SUMS
            self.acc_sumLeft -= removeLeft.y
            self.acc_sumLeft += removeRight.y
            self.acc_sumRight -= removeRight.y
            self.acc_sumRight += accvalue.y

            #AVG
            avgLeft = self.acc_sumLeft / (Settings.DETECTOR_BUFFER_SIZE / 2)
            avgRight = self.acc_sumRight / (Settings.DETECTOR_BUFFER_SIZE / 2)

            #THRESHOLD
            trigger = abs(avgLeft - avgRight) > Settings.DETECTOR_ACC_X_THRESHOLD

            if trigger:
                if self.train_detected_accel == False:
                    self.acc_holding_counter = Settings.DETECTOR_ACC_HOLDING_PERIOD
                    self.statusled.setLedValue(0x000030)
                    #pycom.rgbled(0x003000)
                    print('Train acc start mag measurement')
                    self.train_detected_accel = True
            #check timeout
            if self.train_detected_accel == True:
                self.acc_holding_counter -=1
                if self.acc_holding_counter == 0:
                    self.statusled.setLedValue(0x000000)
                    print('Train acc timeout, stop mag measurement')
                    #self.train_detection.upload.add_measurement(self.mag_measurement)
                    self.acc_measurement = None
                    self.acc_holding_counter = 0
                    self.train_detected_accel = False


    def add_value(self, value):
        #print("mag counter : " + '{}'.format(self.mag_holding_counter))
        self.mag_buffer.append(value)
        bufferLength = len(self.mag_buffer)
        if bufferLength < Settings.DETECTOR_BUFFER_SIZE / 2:
            self.mag_sumLeft += value.vector
        elif bufferLength <= Settings.DETECTOR_BUFFER_SIZE:
            self.mag_sumRight += value.vector
        else:
            #UPDATE BUFFER
            removeLeft = self.mag_buffer[0]
            removeRight = self.mag_buffer[int(Settings.DETECTOR_BUFFER_SIZE / 2)]
            self.mag_buffer.pop(0)

            #UPDATE SUMS
            self.mag_sumLeft -= removeLeft.vector
            self.mag_sumLeft += removeRight.vector
            self.mag_sumRight -= removeRight.vector
            self.mag_sumRight += value.vector

            #AVG
            avgLeft = self.mag_sumLeft / (Settings.DETECTOR_BUFFER_SIZE / 2)
            avgRight = self.mag_sumRight / (Settings.DETECTOR_BUFFER_SIZE / 2)

            #THRESHOLD
            trigger = abs(avgLeft - avgRight) > Settings.DETECTOR_THRESHOLD

            #MEASUREMENT if train detected by accelerometer
            if self.train_detected_accel == True:
                if self.mag_holding_counter == 0:
                    if self.mag_measurement == None:
                        if trigger:
                            self.statusled.setLedValue(0x300030)
                            #pycom.rgbled(0x003000)
                            print('Start mag measurement')
                            self.mag_measurement = Measurement(self.train_detection.device_id,self.train_detection.rtc.now())
                            for x in self.mag_buffer:
                                self.mag_measurement.add_value(x)
                            self.mag_holding_counter = Settings.DETECTOR_HOLDING_PERIOD
                            self.acc_holding_counter = Settings.DETECTOR_ACC_HOLDING_PERIOD
                        else:
                            self.statusled.setLedValue(0x300000)

                    else:
                        self.statusled.setLedValue(0x000000)
                        print('Stop measurement')
                        self.train_detection.upload.add_measurement(self.mag_measurement)
                        self.mag_measurement = None
                        self.train_detected_accel = False
                else:
                    if len(self.mag_measurement.values) < Settings.DETECTOR_MAX_MEASUREMENT_LENGTH:
                        self.mag_measurement.add_value(value)
                        if trigger:
                            self.mag_holding_counter = Settings.DETECTOR_HOLDING_PERIOD
                            self.acc_holding_counter = Settings.DETECTOR_ACC_HOLDING_PERIOD
                            self.statusled.setLedValue(0x300030)
                        else:
                            self.mag_holding_counter -= 1
                            self.statusled.setLedValue(0x300000)
                    else:
                        self.statusled.setLedValue(0x000000)
                        print('Abort measurement')
                        self.train_detection.upload.add_measurement(self.mag_measurement)
                        self.mag_measurement = None
                        self.mag_holding_counter = 0
                        self.train_detected_accel = False

    def add_acc_value(self, value):
        self.acc_buffer.append(value)
        bufferLength = len(self.acc_buffer)
        if bufferLength < Settings.DETECTOR_BUFFER_SIZE / 2:
            self.acc_sumLeft += value.x
        elif bufferLength <= Settings.DETECTOR_BUFFER_SIZE:
            self.acc_sumRight += value.x
        else:
            #UPDATE BUFFER
            removeLeft = self.acc_buffer[0]
            removeRight = self.acc_buffer[int(Settings.DETECTOR_BUFFER_SIZE / 2)]
            self.acc_buffer.pop(0)

            #UPDATE SUMS
            self.acc_sumLeft -= removeLeft.x
            self.acc_sumLeft += removeRight.x
            self.acc_sumRight -= removeRight.x
            self.acc_sumRight += value.x

            #AVG
            avgLeft = self.acc_sumLeft / (Settings.DETECTOR_BUFFER_SIZE / 2)
            avgRight = self.acc_sumRight / (Settings.DETECTOR_BUFFER_SIZE / 2)

            #THRESHOLD
            trigger = abs(avgLeft - avgRight) > Settings.DETECTOR_ACC_X_THRESHOLD

            #MEASUREMENT
            if self.acc_holding_counter == 0:
                if self.acc_measurement == None:
                    if trigger:
                        self.statusled.setLedValue(0x000030)
                        #pycom.rgbled(0x003000)
                        print('Start acc measurement')
                        self.acc_measurement = Measurement(self.train_detection.device_id,self.train_detection.rtc.now())
                        for x in self.acc_buffer:
                            self.acc_measurement.add_value(x)
                        self.acc_holding_counter = Settings.DETECTOR_HOLDING_PERIOD
                else:
                    self.statusled.clearLedValue(0x000030)
                    print('Stop acc measurement')
                    self.train_detection.upload.add_measurement(self.acc_measurement)
                    self.acc_measurement = None
            else:
                if len(self.acc_measurement.values) < Settings.DETECTOR_MAX_MEASUREMENT_LENGTH:
                    self.acc_measurement.add_value(value)
                    if trigger:
                        self.acc_holding_counter = Settings.DETECTOR_HOLDING_PERIOD
                    else:
                        self.acc_holding_counter -= 1
                else:
                    self.statusled.clearLedValue(0x000030)
                    print('Abort acc measurement')
                    self.train_detection.upload.add_measurement(self.acc_measurement)
                    self.acc_measurement = None
                    self.acc_holding_counter = 0
