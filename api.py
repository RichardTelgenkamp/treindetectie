import usocket
import os
import ujson
from settings import Settings
from api_result import ApiResult

class Api:

    def __init__(self):
        self.host = Settings.API_BASE_URL
        self.port = Settings.API_PORT
        self.timeout = Settings.API_SOCKET_TIMEOUT

    def http_get(self, path):
        try:
            sckt = usocket.socket()
            sckt.settimeout(self.timeout)
            sckt.connect(usocket.getaddrinfo(self.host, self.port)[0][-1])
            sckt.write(b"GET %s HTTP/1.0\r\n" % path)
            sckt.write(b"Host: %s\r\n" % self.host)
            sckt.write(b"\r\n")
            data = sckt.read()
            sckt.close()
            return self.get_api_result(data)
        except Exception as e:
            print("Api error: " + str(e))
            return ApiResult()

    def http_post(self, path, json):
        try:
            sckt = usocket.socket()
            sckt.settimeout(self.timeout)
            sckt.connect(usocket.getaddrinfo(self.host, self.port)[0][-1])
            sckt.write(b"POST %s HTTP/1.0\r\n" % path)
            sckt.write(b"Host: %s\r\n" % self.host)
            sckt.write(b"Content-Type: application/json\r\n")
            sckt.write(b"Content-Length: %d\r\n" % len(json))
            sckt.write(b"\r\n")
            sckt.write(json)
            data = sckt.read()
            sckt.close()
            return self.get_api_result(data)
        except Exception as e:
            print("Api error: " + str(e))
            return ApiResult()

    def post_status(self, status_dict):
        json = ujson.dumps(status_dict)
        result = self.http_post('/api/pycom/status', json)
        if result.ok:
            return ujson.loads(result.content)
        else:
            return None

    def get_downloads(self, device_id):
        result = self.http_get('/api/pycom/downloads?id={}'.format(device_id))
        if result.ok:
            return ujson.loads(result.content)
        return list()

    def get_download(self, id):
        result = self.http_get('/api/pycom/download?id={}'.format(id))
        if result.ok:
            return ujson.loads(result.content)
        else:
            return None

    def post_measurement(self, measurement_file):
        try:
            chunksize = 100
            path = '/api/pycom/traindetection'
            size = os.stat(measurement_file)[6]

            sckt = usocket.socket()
            sckt.settimeout(self.timeout)
            sckt.connect(usocket.getaddrinfo(self.host, self.port)[0][-1])
            sckt.write(b"POST %s HTTP/1.0\r\n" % path)
            sckt.write(b"Host: %s\r\n" % self.host)
            sckt.write(b"Content-Type: application/json\r\n")
            sckt.write(b"Content-Length: %d\r\n" % size)
            sckt.write(b"\r\n")

            with open(measurement_file, 'rb') as f:
                while True:
                    chunk = f.read(chunksize)
                    if chunk:
                        sckt.write(chunk)
                    else:
                        break
            data = sckt.read()
            sckt.close()
            return self.get_api_result(data)
        except Exception as e:
            print("Api error: " + str(e))
            return ApiResult()

    def get_api_result(self, data):
        result = ApiResult()
        if data != None:
            lines = data.decode("utf-8").split('\r\n')
            contentIndex = -1

            for x in range(len(lines)):
                if x == 0:
                    if lines[x].endswith('200 OK'):
                        result.ok = True
                        result.code = 200
                    elif lines[x].endswith('400 Bad Request'):
                        result.code = 400
                elif x == contentIndex:
                    result.content = lines[x]
                elif lines[x] == '':
                    contentIndex = x + 1
        else:
            print('data == None')

        return result
