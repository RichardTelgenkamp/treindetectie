import pycom

class StatusLed:
    def __init__(self, ledvalue):
        self.ledvalue = 0

    def setLedValue(self, setvalue):
        self.ledvalue = setvalue
        #print('{}'.format(self.ledvalue))
        pycom.rgbled(self.ledvalue)

    def clearLedValue(self, setvalue):
        self.ledvalue -= setvalue
        #print('{}'.format(self.ledvalue))
        pycom.rgbled(self.ledvalue)
