import _thread
import os
import utime
import ujson

class Upload:

    def __init__(self, api):
        self.measurements = list()
        self.writeToFile = list()
        self.api = api
        self.dir = '/sd/measurements'

        ls = os.listdir('/sd')
        if ls.count('measurements') == 0:
            os.mkdir(self.dir)

        ls = os.listdir(self.dir)
        for f in ls:
            self.measurements.append('{}/{}'.format(self.dir, f))

    def start(self):
         _thread.start_new_thread(Upload.upload_loop, (self.measurements, self.writeToFile, self.dir, self.api))

    def upload_loop(measurements, writeToFile, dir, api):
        while True:
            try:
                if len(writeToFile) > 0:
                    measurement = writeToFile[0]
                    measurement.write_to_file(dir)
                    print("writing to file...done")
                    measurements.append(measurement.file_name)

                # RT 28-9-2018 upload by LTE-M not possible

                #     writeToFile.remove(measurement)
                # if len(measurements) > 0:
                #     file_name = measurements[0]
                #     print('Upload {}'.format(file_name))
                #     result = api.post_measurement(file_name)
                #     if result.ok or result.code == 400:
                #         print('Upload done ({}), delete file'.format(result.code))
                #         os.remove(file_name)
                #         measurements.remove(file_name)
                #     else:
                #         print('Upload Failed')
            writeToFile.remove(measurement)
                if len(measurements) > 0:
                    file_name = measurements[0]
                    print('Upload {}'.format(file_name))
                    result = api.post_measurement(file_name)
                    if result.ok or result.code == 400:
                        print('Upload done ({}), delete file'.format(result.code))
                        os.remove(file_name)
                        measurements.remove(file_name)
                    else:
                        print('Upload Failed')

            except Exception as e:
                print("Upload error: " + str(e))

            utime.sleep(5)

    def add_measurement(self, measurement):
        print('Write measurement to file')
        self.writeToFile.append(measurement)
